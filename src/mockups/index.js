import ROVER_CAMERAS from "./roverCameras.json";
import ROVERS from "./rovers.json";

export { ROVER_CAMERAS, ROVERS };
