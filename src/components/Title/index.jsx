import { styles } from "./styles";

const Title = ({ children }) => <p style={styles.title}>{children}</p>;

export { Title };
