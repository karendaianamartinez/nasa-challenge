import { THEMES } from "../../styles";

const styles = {
  title: {
    fontSize: 18,
    fontWeight: "bold",
    textTransform: "uppercase",
    color: THEMES.COLORS.WHITE,
  },
};

export { styles };
