const Select = ({
  name,
  register,
  options,
  multiple,
  defaultValue,
  styles,
}) => (
  <select
    {...register}
    defaultValue={defaultValue}
    multiple={multiple}
    name={name}
    style={styles}
  >
    <option disabled selected value>
      Select an option
    </option>

    {options.map((option, i) => (
      <option key={i} value={option.value}>
        {option.name}
      </option>
    ))}
  </select>
);

export { Select };
