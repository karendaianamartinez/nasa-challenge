import { styles } from "./styles";

const Pagination = ({
  handlePreviousPage,
  handleNextPage,
  currentPage,
  pageSize,
  totalCount,
}) => (
  <div style={styles.container}>
    <span style={styles.text}>Current Page: {currentPage}</span>

    <button disabled={currentPage === 1} onClick={handlePreviousPage}>
      Previous Page
    </button>
    <button disabled={totalCount < pageSize} onClick={handleNextPage}>
      Next Page
    </button>
  </div>
);

export { Pagination };
