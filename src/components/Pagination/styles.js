import { THEMES } from "../../styles";

const styles = {
  container: {
    display: "flex",
    flex: 1,
    justifyContent: "center",
    marginTop: 100,
  },
  text: {
    color: THEMES.COLORS.WHITE,
    marginRight: 10,
  },
};

export { styles };
