const DateInput = ({
  name,
  register,
  value,
  defaultValue,
  maxDate,
  minDate,
  styles,
}) => (
  <input
    style={styles}
    {...register}
    defaultValue={defaultValue}
    max={maxDate}
    min={minDate}
    name={name}
    type="date"
    value={value}
  />
);

export { DateInput };
