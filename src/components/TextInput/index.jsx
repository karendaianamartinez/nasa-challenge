const TextInput = ({ name, register, defaultValue, styles }) => (
  <input
    defaultValue={defaultValue}
    name={name}
    type="text"
    {...register}
    style={styles}
  />
);

export { TextInput };
