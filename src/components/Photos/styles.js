const styles = {
  container: {
    display: "flex",
    flexWrap: "wrap",
    flexDirection: "row",
    justifyContent: "space-evenly",
    padding: 12,
  },
  image: {
    width: 300,
    height: 300,
    margin: 12,
  },
};

export { styles };
