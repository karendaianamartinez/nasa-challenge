import { styles } from "./styles";

const Photos = ({ photos }) => {
  return (
    <div style={styles.container}>
      {photos && photos.length
        ? photos.map((photo) => (
            <img
              alt={photo.img_src}
              key={photo.id}
              src={photo.img_src}
              style={styles.image}
            />
          ))
        : null}
    </div>
  );
};

export { Photos };
