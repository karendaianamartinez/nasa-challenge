import { styles } from "./styles";

const Button = ({ text, onClick }) => (
  <button onClick={onClick} style={styles.button}>
    {text}
  </button>
);

export { Button };
