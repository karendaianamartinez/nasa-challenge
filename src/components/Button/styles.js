import { THEMES } from "../../styles";

const styles = {
  button: {
    width: 250,
    height: 40,
    cursor: "pointer",
    margin: 6,
    color: THEMES.COLORS.BLACK,
    backgroundColor: THEMES.COLORS.GRAY_MEDIUM,
    borderRadius: 18,
  },
};

export { styles };
