import { THEMES } from "../../styles";

const styles = {
  container: {
    display: "flex",
    flex: 1,
    alignItems: "center",
    flexDirection: "row",
    backgroundColor: THEMES.COLORS.GRAY_MEDIUM,
  },
  link: {
    margin: 12,
  },
  title: {
    flex: 1,
    fontSize: 24,
    fontWeight: "bold",
    textTransform: "uppercase",
    margin: 24,
  },
};

export { styles };
