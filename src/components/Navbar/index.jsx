import { Link } from "react-router-dom";

import { styles } from "./styles";

const Navbar = () => (
  <div style={styles.container}>
    <p style={styles.title}>NASA API</p>

    <Link style={styles.link} to="/explore-rovers">
      <p>Home</p>
    </Link>

    <Link style={styles.link} to="/favorites">
      <p>Favorites</p>
    </Link>
  </div>
);

export { Navbar };
