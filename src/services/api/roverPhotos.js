import axios from "axios";

export const fetchRoverPhotos = async ({
  sol,
  camera,
  earth_date,
  name,
  page,
}) => {
  const { data } = await axios.get(
    `/mars-photos/api/v1/rovers/${name}/photos?`,
    {
      params: {
        sol,
        camera,
        earth_date,
        page,
      },
    }
  );

  return data;
};

export const fetchLatestRoverPhotos = async ({ earth_date, name, page }) => {
  const { data } = await axios.get(
    `/mars-photos/api/v1/rovers/${name}/latest_photos?`,
    { params: { earth_date, page } }
  );

  return data;
};
