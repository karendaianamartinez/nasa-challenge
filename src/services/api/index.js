import axios from "axios";

import { config } from "../../config";

export { fetchRoverPhotos, fetchLatestRoverPhotos } from "./roverPhotos";

axios.defaults.baseURL = config.NASA_API_URL;
axios.defaults.params = { api_key: config.NASA_API_KEY };
