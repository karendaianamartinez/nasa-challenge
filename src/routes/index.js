import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import { ExploreRovers, Favorites, Rover } from "../features";
import { Navbar } from "../components";

const routes = [
  {
    path: "/explore-rovers",
    component: ExploreRovers,
  },
  {
    path: "/favorites",
    component: Favorites,
  },
  {
    path: "/rover/:name/:id?",
    component: Rover,
  },
];

const Routes = () => (
  <>
    <Navbar />
    <Switch>
      {routes.map((route, i) => (
        <Route exact key={i} {...route} />
      ))}

      <Route exact path="*">
        <Redirect to="/explore-rovers" />
      </Route>
    </Switch>
  </>
);

export { Routes };
