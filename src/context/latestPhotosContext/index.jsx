import { createContext, useReducer } from "react";

import { SET_SHOW_LATEST_PHOTOS } from "../../constants";

const initialState = {
  curiosity: true,
  opportunity: true,
  spirit: true,
};

const LatestPhotosContext = createContext({
  state: initialState,
  dispatch: () => null,
});

const reducer = (state, action) => {
  switch (action.type) {
    case SET_SHOW_LATEST_PHOTOS:
      return {
        ...state,
        [action.payload]: false,
      };
    default:
      return state;
  }
};

const LatestPhotosProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <LatestPhotosContext.Provider value={{ state, dispatch }}>
      {children}
    </LatestPhotosContext.Provider>
  );
};

export { LatestPhotosContext, LatestPhotosProvider };
