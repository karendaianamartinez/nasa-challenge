import { createContext, useReducer, useEffect } from "react";

import { getLocalStorage, setLocalStorage } from "../../services";
import { ADD_FAVORITE_ROVER_PARAMS } from "../../constants";

const initialState = {
  favoriteParams: [],
};

const RoverPhotosContext = createContext({
  state: initialState,
  dispatch: () => null,
});

const reducer = (state, action) => {
  switch (action.type) {
    case ADD_FAVORITE_ROVER_PARAMS:
      return {
        ...state,
        favoriteParams: [
          ...state.favoriteParams,
          { id: state.favoriteParams.length, ...action.payload },
        ],
      };
    default:
      return state;
  }
};

const RoverPhotosProvider = ({ children }) => {
  const storage = getLocalStorage("state", initialState);
  const [state, dispatch] = useReducer(reducer, storage);

  useEffect(() => {
    setLocalStorage("state", state);
  }, [state]);

  return (
    <RoverPhotosContext.Provider value={{ state, dispatch }}>
      {children}
    </RoverPhotosContext.Provider>
  );
};

export { RoverPhotosContext, RoverPhotosProvider };
