import { BrowserRouter } from "react-router-dom";
import { QueryClient, QueryClientProvider } from "react-query";

import { RoverPhotosProvider, LatestPhotosProvider } from "./context";
import { Routes } from "./routes";

const queryClient = new QueryClient();

const App = () => (
  <QueryClientProvider client={queryClient}>
    <LatestPhotosProvider>
      <RoverPhotosProvider>
        <BrowserRouter>
          <Routes />
        </BrowserRouter>
      </RoverPhotosProvider>
    </LatestPhotosProvider>
  </QueryClientProvider>
);

export { App };
