const config = {
  NASA_API_URL: process.env.REACT_APP_NASA_API_URL,
  NASA_API_KEY: process.env.REACT_APP_NASA_API_KEY,
};

export { config };
