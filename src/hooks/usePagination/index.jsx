import { useState } from "react";

const usePagination = () => {
  const [page, setPage] = useState(1);

  const handleNextPage = () => setPage(page + 1);
  const handlePreviousPage = () => setPage(page - 1);

  return {
    handleNextPage,
    handlePreviousPage,
    page,
  };
};

export { usePagination };
