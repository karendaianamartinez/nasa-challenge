import { useContext } from "react";
import { useQuery } from "react-query";
import { useForm } from "react-hook-form";
import { useParams } from "react-router-dom";
import moment from "moment";

import { usePagination } from "../../../hooks";
import { fetchRoverPhotos, fetchLatestRoverPhotos } from "../../../services";
import { RoverPhotosContext, LatestPhotosContext } from "../../../context";
import {
  Button,
  DateInput,
  Loading,
  Pagination,
  Photos,
  Select,
  TextInput,
  Title,
} from "../../../components";
import { styles } from "./styles";
import { ROVER_CAMERAS } from "../../../mockups";
import {
  ADD_FAVORITE_ROVER_PARAMS,
  SET_SHOW_LATEST_PHOTOS,
} from "../../../constants";

const Rover = () => {
  const { name, id } = useParams();
  const { register, handleSubmit, getValues } = useForm();
  const { handleNextPage, handlePreviousPage, page } = usePagination();
  const {
    state: { favoriteParams },
    dispatch,
  } = useContext(RoverPhotosContext);
  const { state: latestPhotosState, dispatch: latestPhotosDispatch } =
    useContext(LatestPhotosContext);
  const currentDate = moment().format("YYYY-MM-DD");

  // fetch latest rover photos
  const { data: latestRoverPhotos, isFetching: latestPhotosIsLoading } =
    useQuery(
      ["latestRoverPhotos", name, page],
      () =>
        fetchLatestRoverPhotos({
          name,
          earth_date: currentDate,
          page,
        }),
      {
        refetchOnWindowFocus: false,
        enabled: !id,
      }
    );

  // fetch rover photos
  const {
    data: roverPhotos,
    isFetching: roverPhotosIsFetching,
    refetch,
  } = useQuery(
    ["roverPhotos", name, page],
    () => fetchRoverPhotos({ name, page, ...getValues() }),
    {
      refetchOnWindowFocus: false,
      enabled: !!id || !latestPhotosState[name],
      onSuccess: () =>
        latestPhotosState[name] &&
        !id &&
        latestPhotosDispatch({
          type: SET_SHOW_LATEST_PHOTOS,
          payload: name,
        }),
    }
  );

  const handleAddFavoriteParams = () => {
    dispatch({
      type: ADD_FAVORITE_ROVER_PARAMS,
      payload: { name, ...getValues() },
    });
  };

  const onSubmit = (data) => refetch(data);

  return (
    <div style={styles.container}>
      <Title>Filter rover by parameters</Title>

      <div style={styles.formContainer}>
        <TextInput
          defaultValue={id && favoriteParams[id]?.sol}
          name="sol"
          register={{ ...register("sol", { required: true }) }}
          styles={styles.input}
        />
        <Select
          defaultValue={id && favoriteParams[id]?.camera}
          name="camera"
          options={ROVER_CAMERAS}
          register={{ ...register("camera") }}
          styles={styles.input}
        />
        <DateInput
          defaultValue={id && favoriteParams[id]?.earth_date}
          name="earth_date"
          register={{ ...register("earth_date") }}
          styles={styles.input}
        />
      </div>

      <Button onClick={handleAddFavoriteParams} text="Add favorite params" />

      <Button onClick={handleSubmit(onSubmit)} text="Get rover" />

      <Pagination
        currentPage={page}
        handleNextPage={handleNextPage}
        handlePreviousPage={handlePreviousPage}
        pageSize={25}
        totalCount={
          latestPhotosState[name]
            ? latestRoverPhotos?.latest_photos?.length
            : roverPhotos?.photos?.length
        }
      />

      {latestPhotosState[name] && !id && (
        <Title>Latest photos of the day {currentDate}</Title>
      )}

      {roverPhotosIsFetching || latestPhotosIsLoading ? (
        <Loading />
      ) : (
        <Photos
          photos={
            latestPhotosState[name] && !id
              ? latestRoverPhotos?.latest_photos
              : roverPhotos?.photos
          }
        />
      )}
    </div>
  );
};

export { Rover };
