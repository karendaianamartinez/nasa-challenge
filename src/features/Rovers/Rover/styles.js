import { THEMES } from "../../../styles";

const styles = {
  container: {
    display: "flex",
    flex: 1,
    flexDirection: "column",
    backgroundColor: THEMES.COLORS.BLACK,
    padding: 24,
  },
  formContainer: {
    display: "flex",
    flex: 1,
    flexWrap: "wrap",
  },
  input: {
    width: 300,
    height: 35,
    cursor: "pointer",
    margin: 6,
    color: THEMES.COLORS.BLACK,
    backgroundColor: THEMES.COLORS.WHITE,
  },
  photosContainer: {
    display: "flex",
    flex: 1,
    flexWrap: "wrap",
    flexDirection: "column",
  },
};

export { styles };
