const Photo = ({ img_src }) => (
  <div>
    <img src={img_src} />
  </div>
);

export { Photo };
