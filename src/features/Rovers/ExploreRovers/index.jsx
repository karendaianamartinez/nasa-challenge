import { useHistory } from "react-router-dom";

import { styles } from "./styles";
import { ROVERS } from "../../../mockups";
import { RoverPhoto } from "./components";
import { Title } from "../../../components";

const ExploreRovers = () => {
  const history = useHistory();

  const handleRoverRedirect = (name) => history.push(`/rover/${name}`);

  return (
    <div style={styles.container}>
      <Title>Choose a Rover to Explore</Title>

      <div style={styles.roversContainer}>
        {ROVERS.map((rover, i) => (
          <RoverPhoto
            key={i}
            {...rover}
            onClick={() => handleRoverRedirect(rover.value)}
          />
        ))}
      </div>
    </div>
  );
};
export { ExploreRovers };
