import { styles } from "./styles";

const RoverPhoto = ({ name, image, onClick }) => (
  <div onClick={onClick} style={styles.roverContainer}>
    <p style={styles.name}>{name}</p>
    <img src={image} style={styles.roverImage} />
  </div>
);

export { RoverPhoto };
