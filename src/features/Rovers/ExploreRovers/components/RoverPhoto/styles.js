import { THEMES } from "../../../../../styles";

const styles = {
  name: {
    fontSize: 16,
    fontWeight: "bold",
    textTransform: "uppercase",
    color: THEMES.COLORS.WHITE,
  },
  roverContainer: {
    display: "flex",
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    cursor: "pointer",
    margin: 12,
    padding: 24,
    border: `${THEMES.COLORS.WHITE} 1px solid`,
    borderRadius: 24,
  },
  roverImage: {
    width: 250,
    height: 250,
    objectFit: "cover",
    borderRadius: 125,
  },
};

export { styles };
