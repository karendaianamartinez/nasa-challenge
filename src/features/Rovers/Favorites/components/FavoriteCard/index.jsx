import { styles } from "./styles";

const Label = ({ label, value }) => (
  <div style={styles.labelContainer}>
    <p style={styles.label}>{label}:</p>
    <p>{value}</p>
  </div>
);

const FavoriteCard = ({ name, sol, camera, earth_date, onClick }) => (
  <div onClick={onClick} style={styles.container}>
    <Label label="Name" value={name} />
    <Label label="Sol" value={sol} />
    <Label label="Camera" value={camera} />
    <Label label="Earth date" value={earth_date} />
  </div>
);

export { FavoriteCard };
