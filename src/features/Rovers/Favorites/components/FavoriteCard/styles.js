import { THEMES } from "../../../../../styles/themes";

const styles = {
  container: {
    display: "flex",
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    backgroundColor: THEMES.COLORS.GRAY_MEDIUM,
    margin: 12,
    padding: 6,
    cursor: "pointer",
    borderRadius: 6,
  },
  label: {
    textTransform: "uppercase",
    fontWeight: "bold",
    marginRight: 6,
  },
  labelContainer: {
    display: "flex",
    flex: 1,
    flexDirection: "row",
    margin: 12,
  },
};

export { styles };
