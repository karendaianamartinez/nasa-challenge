import { THEMES } from "../../../styles";

const styles = {
  container: {
    display: "flex",
    flex: 1,
    flexDirection: "column",
    backgroundColor: THEMES.COLORS.BLACK,
    padding: 24,
  },
};

export { styles };
