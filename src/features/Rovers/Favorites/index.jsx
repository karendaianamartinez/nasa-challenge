import { useContext } from "react";
import { useHistory } from "react-router";

import { RoverPhotosContext } from "../../../context";
import { FavoriteCard } from "./components";
import { Title } from "../../../components";
import { styles } from "./styles";

const Favorites = () => {
  const history = useHistory();
  const {
    state: { favoriteParams },
  } = useContext(RoverPhotosContext);

  const handleRedirectRover = ({ id, name }) => {
    history.push(`/rover/${name}/${id}`);
  };

  return (
    <div style={styles.container}>
      <Title>Favorites params</Title>

      {favoriteParams.map((favorite, i) => (
        <FavoriteCard
          key={i}
          {...favorite}
          onClick={() =>
            handleRedirectRover({ id: favorite.id, name: favorite.name })
          }
        />
      ))}
    </div>
  );
};

export { Favorites };
