const COLORS = {
  WHITE: "#FFFFFF",
  BLACK: "#000000",
  GRAY: "#9F9E9E",
  GRAY_DARK: "#707070",
  GRAY_LIGHT: "#F9F9F9",
  GRAY_MEDIUM: "#E4E4E4",
  ERRORS_RED: "red",
};

export const THEMES = {
  COLORS,
};
