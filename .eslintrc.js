module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
    jest: true,
  },
  extends: [
    "eslint:recommended",
    "plugin:react/recommended",
    "plugin:import/errors",
    "prettier",
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: "module",
  },
  plugins: ["react", "prettier"],
  rules: {
    "react/jsx-sort-props": 1,
    "react/prop-types": 0,
    "react/react-in-jsx-scope": 0,
    "react/jsx-props-no-spreading": 0,
    "react/jsx-boolean-value": "error",
    "import/prefer-default-export": 0,
    "import/no-default-export": "error",
    // eslint-plugin-prettier
    "prettier/prettier": "error",
    // eslint-plugin-import
    "import/order": [
      "error",
      {
        groups: [
          "builtin",
          "external",
          "internal",
          ["sibling", "parent", "index"],
        ],
        "newlines-between": "always",
      },
    ],
    "import/named": "off",
    "import/namespace": "off",
    "import/default": "off",
    "import/no-named-as-default-member": "off",
    // eslint-plugin-react
    "react/jsx-filename-extension": [1, { extensions: [".jsx", ".js"] }],
    // eslint
    "import/extensions": 0,
    "no-param-reassign": [
      "error",
      {
        props: true,
        ignorePropertyModificationsFor: ["state"],
      },
    ],
    "no-return-await": "error",
    eqeqeq: "error",
    "no-unneeded-ternary": "error",
    "no-console": 1,
  },
};
