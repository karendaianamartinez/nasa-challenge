# Nasa challenge

This project allows to obtain the photos of the mars rover through the api of nasa.

## Installation

Install nasa-challenge with npm start

```bash
  npm install
```

Start nasa-challenge with npm start

```bash
  npm start
```

## Environment Variables

To run this project, you will need to add the following environment variables to your .env file. Check .env.template file and replace your variables.

`REACT_APP_NASA_API_URL`

`REACT_APP_NASA_API_KEY`

## Project Structure

``

- components
  - index.js
  - Button
  - DateInput
- config
  - index.js
- constants
  - index.js
- context
  - index.js
  - latestPhotosContext
  - roverPhotosContext
- features
  - index.js
  - Rovers
    - ExploreRovers
    - Favorites
- hooks
  - index.js
  - usePagination.js
- routes
  - index.js
- services
  - index.js
  - api
  - storage
- styles

  - themes
  - index.js
    ``

- **Components:** Place all common components used throughout the app. Feature scoped components should be placed in a `components` folder within said feature.

- **Config:** Global configuration including env variables or other defined configurations params are defined here

- **Constants:** All common constants should be placed here. This will probably hold business logic related stuff such as certain names or maps used in the entire app. Constants scoped to a sections of the app should be placed under a `constants` folder within that section.

- **Context:** Global contexts to handle state management are placed here. It also includes a `ContextProviders` component to render all Providers and avoid overpopulating `App` component with them

- **Features:** A characteristic defines a part of the application and inside it contains the different related screens.

- **Hooks:** Place all common hooks here.

- **Routes:** All application routes are defined.

- **Services:** Global services that provide specific logic for third party libraries or to manipulate a global components, app state, etc should be placed here.

- **Styles:** All common styles should be placed here, e.g. `fonts` or `themes`.

## API Reference

https://api.nasa.gov/

#### Get all the photos of a rover

```http
  GET /api/mars-photos/api/v1/rovers/${name}/photos?sol=${sol}&camera=${camera}&api_key=${api_key}
```

| Parameter    | Type     | Description                                                |
| :----------- | :------- | :--------------------------------------------------------- |
| `api_key`    | `string` | **Required**. Your API key                                 |
| `name`       | `string` | **Required**. Rover name                                   |
| `sol`        | `string` | **Required**. Sol (ranges from 0 to max found in endpoint) |
| `camera`     | `string` | Camera                                                     |
| `earth_date` | `date`   | Corresponding date on earth for the given sol              |
| `page`       | `number` | 25 items per page returned                                 |

#### Get the latest photos of a rover in current date

```http
  GET /api/mars-photos/api/v1/rovers/${name}/latest_photos?earth_date=${earth_date}&api_key=${api_key}
```

| Parameter    | Type     | Description                                   |
| :----------- | :------- | :-------------------------------------------- |
| `api_key`    | `string` | **Required**. Your API key                    |
| `name`       | `string` | **Required**. Rover name                      |
| `earth_date` | `date`   | Corresponding date on earth for the given sol |
| `page`       | `number` | 25 items per page returned                    |

## Contributing

- Hi, I'm **Karen Daiana Martinez**! 👋 - Software developer - [Profile Linkedin](https://www.linkedin.com/in/karen-martinez-9ab059185)
